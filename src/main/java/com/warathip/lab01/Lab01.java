/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.warathip.lab01;

import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class Lab01 {

    public static void main(String[] args) {
        System.out.println("Welcome to ox Program");
        int n = 3;
        char[][] table = new char[n][n];
        for(int i =0;i<n;i++){
            for(int j=0;j<n;j++){
                table[i][j]='-';
            }
        }
        Scanner in = new Scanner(System.in);
        boolean a1 = true;
        boolean gameend = false;
        while(!gameend){
            drawtable(table);
        
        if(a1){
            System.out.println("x turn");
        }else{
            System.out.println("o turn");
        }
        
        char c = '-';
        if(a1){
            c='x';
        }else{
            c='o';
        }
        
        int row;
        int col;
        
        while(true){
            System.out.print("Please input row,col : ");
            row = in.nextInt()-1;
            col = in.nextInt()-1;
            
            if(row<0||col<0||row>=n||col>=n){
                System.out.println("This position is off the bounds of the board! Try again. ");
            }else if(table[row][col]!='-'){
                System.out.println("Someone has already made a move at this position! Try again. ");
            }else{
                break;
            }
        }
        table[row][col]=c;
        
            switch (winner(table)) {
                case 'x' -> {
                    drawtable(table);
                    System.out.println("x win!!");
                    gameend = true;
                }
                case 'o' -> {
                    drawtable(table);
                    System.out.println("o win!!");
                    gameend = true;
                }
                default -> {
                    if(tableFull(table)){
                        System.out.println("It's a tie!");
                        gameend = true;
                    }else{
                        a1 = !a1;
                    }
                }
            }
        }
        
        
    }

    private static void drawtable(char[][] table) {
        for (char[] table1 : table) {
            for (int j = 0; j < table1.length; j++) {
                System.out.print(table1[j]);
            }
            System.out.println();
        }
    }

    private static boolean tableFull(char[][] table) {
        for (char[] table1 : table) {
            for (int j = 0; j < table1.length; j++) {
                if (table1[j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static char winner(char[][] table) {
        for (char[] table1 : table) {
            boolean inARow = true;
            char value = table1[0];
            if (value == '-') {
                inARow = false;
            } else {
                for (int j = 1; j < table1.length; j++) {
                    if (table1[j] != value) {
                        inARow = false;
                        break;
                    }
                }
            }
            if(inARow){
                return value;
            }
        }
        for(int i =0;i<table[0].length;i++){
            boolean inACol = true;
            char value = table[0][i];
            if(value=='-'){
                inACol = false;
            }else{
                for(int j =1;j<table.length;j++){
                    if(table[j][i]!=value){
                        inACol = false;
                        break;
                    }
                }
            }
            if(inACol){
                return value;
            }
        }
        boolean inADiag1 = true;
        char value1 = table[0][0];
        if(value1 =='-'){
            inADiag1 = false;
        }else{
            for(int i=1;i<table.length;i++){
                if(table[i][i]!=value1){
                    inADiag1=false;
                    break;
                }
            }
        }
        if(inADiag1){
            return value1;
        }
        
        boolean inADiag2 = true;
        char value2 = table[0][table.length-1];
        
        if(value2 == '-'){
            inADiag2 =false;
        }else{
            for(int i=1;i<table.length;i++){
                if(table[i][table.length-1-i]!=value2){
                    inADiag2 = false;
                    break;
                }
            }
        }
        if(inADiag2){
            return value2;
        }
        return ' ';
        
    }
}
